const functions = require('firebase-functions');
const admin = require('firebase-admin');
const validator = require('validator');
const _ = require('lodash');

const OK = { ok: true };

module.exports = functions.https.onRequest((request, response) => {
    let username = JSON.parse(request.body).username;
    let userDetails = request.authorizedUser;
    if (!validate(username)) {
        response.send({ ok: false, error: 'VALIDATION_FAILED' })
    } else {
        admin.database().ref('usersMap').once('value')
            .then(snapshot => {
                let userList = _.values(snapshot.val());
                let idList = _.keys(snapshot.val());
                if (_.indexOf(userList, username) >= 0) {
                    throw 'NOT_AVAILABLE';
                } else if (_.indexOf(idList, userDetails.uid) >= 0) {   
                    throw 'CANNOT_CHANGE_USERNAME';
                } else {
                    admin.database().ref('usersMap/' + userDetails.uid).set(username);
                }
            }).then(() => { response.send(OK) })
            .catch((error) => { response.send({ ok: false, error: error }) });
    }
});


const validate = (username) => {
    return validator.isLength(username, { min: 6, max: 20 }) && validator.isAlphanumeric(username, ['pl-PL']);
}
