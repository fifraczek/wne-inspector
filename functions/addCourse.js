const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fetch = require('node-fetch');
const google = require('googleapis');

const OK = { ok: true };

module.exports = functions.https.onRequest((request, response) => {
  let courseId = JSON.parse(request.body).id;
  const sheets = google.sheets('v4');
  const coursesMap = {};

  sheets.spreadsheets.values.get({
    key: 'AIzaSyBUSx1cP101Tym3XjF_ynMLRCZwCJ5aAy4',
    spreadsheetId: '1Ongga26piaSvt01Ja5v6GVvRrPxCTG1Mg-bv4LvzQn4',
    range: 'Przedmioty!A2:B',
  }, (err, data) => {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var rows = data.values;
    for (var i = 0; i < rows.length; i++) {
      var row = rows[i];
      coursesMap[row[0]] = row[1];
    }
    let courseName = coursesMap[courseId];
    if (courseName && courseId) {
      admin.database().ref('coursesMap/' + courseId).once('value')
        .then(snapshot => {
          if (snapshot.val()) {
            throw 'ALREADY_EXIST';
          }
        }).then(() => {
          admin.database().ref('coursesMap/' + courseId).set(courseName)
        }).then(() => { response.send(OK) })
        .catch((error) => { response.send({ ok: false, error: error }) });
    } else {
      response.send({ ok: false, error: 'NOT_FOUND' })
    }
  });
});
