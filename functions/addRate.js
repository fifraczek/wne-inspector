const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fetch = require('node-fetch');
const validator = require('validator');
const _ = require('lodash');

const OK = { ok: true };
const COURSE_TYPES = ['W', 'C', 'K'];

module.exports = functions.https.onRequest((request, response) => {
        let rateDetails = JSON.parse(request.body);
        if (!validate(rateDetails)) {
            response.send({ ok: false, error: 'VALIDATION_FAILED' })
        }
        let userDetails = request.authorizedUser;
        let formatedRateDetails = prepareRateDetails(rateDetails, userDetails);
        admin.database().ref('course/' + rateDetails.courseId + '/rates').push(formatedRateDetails)
            .then(() => { response.send(OK) })
            .catch((error) => { response.send({ ok: false, error: error }) });
});



const validate = (json) => {
    if (validator.isLength(json.comment, { min: 0, max: 2500 })
        && validator.isLength(json.teacher, { min: 0, max: 100 })
        && (json.difficulty >= 1 && json.difficulty <= 5)
        && (json.interesting >= 1 && json.interesting <= 5)
        && (json.materials >= 1 && json.materials <= 5)
        && (json.teachersApproach >= 1 && json.teachersApproach <= 5)
        && _.indexOf(COURSE_TYPES, json.courseType) !== -1
    ) {
        return true;
    } else {
        return false;
    }
}

const prepareRateDetails = (json, userDetails) => {
    let result = {};
    result.comment = json.comment;
    result.courseType = json.courseType;
    result.difficulty = json.difficulty;
    result.interesting = json.interesting;
    result.materials = json.materials;
    result.teacher = json.teacher;
    result.teachersApproach = json.teachersApproach;
    result.uid = userDetails.uid;
    result.displayName = userDetails.name;
    result.timestamp = new Date().getTime();
    return result;
}