const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fetch = require('node-fetch');
const validator = require('validator');
const _ = require('lodash');

const OK = { ok: true };

module.exports = functions.https.onRequest((request, response) => {
        let username = JSON.parse(request.body).username;        
        admin.database().ref('usersMap').once('value')
        .then(snapshot => {
            let userList = _.values(snapshot.val());
            if (_.indexOf(userList, username) >= 0) {
                throw 'NOT_AVAILABLE';
            }
        }).then(() => { response.send(OK) })
        .catch((error) => { response.send({ ok: false, error: error }) });
});

