const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const cors = require('cors')({ origin: true });
const _ = require('lodash');

const authorize = (request, response) => {
    let authHeader = request.get('Authorization');
    let authKey = authHeader ? authHeader.split('Bearer ')[1] : null;
    if (authHeader && authKey) {
        return admin.auth().verifyIdToken(authKey);
    } else {
        response.send('unauthorized');
    }
}

const addCourse = require('./addCourse');
exports.addCourse = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        authorize(request, response).then(() => addCourse(request, response));
    });
});

const addRate = require('./addRate');
exports.addRate = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        authorize(request, response).then((userDetails) => {
            request.authorizedUser = userDetails;
            return addRate(request, response);
        }
        );
    });
});

const addFile = require('./addFile');
exports.addFile = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        authorize(request, response).then((userDetails) => {
            request.authorizedUser = userDetails;
            return addFile(request, response);
        }
        );
    });
});

const checkLoginAvailability = require('./checkLoginAvailability');
exports.checkLoginAvailability = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        return checkLoginAvailability(request, response);
    });
});

exports.verifyDomain = functions.auth.user().onCreate(event => {
    const userDetails = event.data;
    const uid = userDetails.uid;
    const email = userDetails.email;
    const allowedDomains = ['student.uw.edu.pl','students.mimuw.edu.pl'];
    let emailDomain = email.split('@')[1];
    if (_.indexOf(allowedDomains, emailDomain) < 0) {
        admin.auth().updateUser(uid, {
            disabled: true
        })
            .then(function (userRecord) {
                console.log("Successfully updated user: ", uid);
            })
            .catch(function (error) {
                console.log("Error updating user:", error);
            });
    }
});

const setLogin = require('./setLogin');
exports.setLogin = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        authorize(request, response).then((userDetails) => {
            request.authorizedUser = userDetails;
            setLogin(request, response);
        });
    });
});
