const functions = require('firebase-functions');
const admin = require('firebase-admin');
const fetch = require('node-fetch');
const validator = require('validator');
const _ = require('lodash');

const OK = { ok: true };
const FILE_TYPES = ['W', 'C', 'K', 'P', 'E', 'I'];


module.exports = functions.https.onRequest((request, response) => {
  let fileDetails = JSON.parse(request.body);
  if (!validate(fileDetails)) {
    response.send({ ok: false, error: 'VALIDATION_FAILED' })
  } else {
    let userDetails = request.authorizedUser;
    let formatedFileDetails = prepareFileDetails(fileDetails, userDetails);
    admin.database().ref('course/' + fileDetails.courseId + '/files').push(formatedFileDetails)
      .then(() => { response.send(OK) })
      .catch((error) => { response.send({ ok: false, error: error }) });
  }
});



const validate = (json) => {
  return validator.isLength(json.description, { min: 0, max: 250 }) && _.indexOf(FILE_TYPES, json.fileType) !== -1;
}

const prepareFileDetails = (json, userDetails) => {
  let metadata = {
    timestamp: new Date().getTime(),
    userId: userDetails.uid,
    displayName: userDetails.name
  }
  let result = _.assign({}, json, metadata);
  delete result.courseId;
  return result;
}