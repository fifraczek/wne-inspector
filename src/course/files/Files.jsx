import './files.css';

import _ from 'lodash';
import React, { Component } from 'react';
import FilesTable from './FilesTable';
import MobileFilesTable from './mobile/MobileFilesTable';
import FileDialog from './fileDialog/FileDialog';
import { Intent } from '@blueprintjs/core';
import { Toast } from '../../components/toast/Toast';
import fetchDatabase from './../../global/utils/fetchDatabase';

const MAX_FILE_SIZE = 50e6;
const MAX_FILE_SIZE_TEXT = 'Maksymalna wielkość pliku wynosi 50 MB'

export default class Files extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newFile: null,
            files: [],
            loadingFiles: true
        };

        this.fileSelected = this.fileSelected.bind(this);
        this.resetFile = this.resetFile.bind(this);
        this.getCourseFiles = this.getCourseFiles.bind(this);
    }

    componentDidMount() {
        this.getCourseFiles();
    }

    getCourseFiles() {
        this.setState({ loadingFiles: true });
        fetchDatabase('course/' + this.props.courseId + '/files', (result) => {
            let resultArray = _.transform(result, (output, value, key) => {
                output.push(_.assign({},value, {id: key, courseId: this.props.courseId}));
              }, []);
              resultArray = _.reverse(_.sortBy(resultArray, ['timestamp']));
            this.setState({ files: resultArray, loadingFiles: false });
        }
        );
    }

    resetFile() {
        this.setState({ newFile: null });
    }

    fileSelected(event) {
        const selectedFile = event.target.files[0];
        if (selectedFile && selectedFile.size > MAX_FILE_SIZE) {
            Toast.show({ message: MAX_FILE_SIZE_TEXT, intent: Intent.DANGER });
        } else {
            this.setState({ newFile: selectedFile });
        }
    };

    renderAddButton() {
        return <div className='file-upload-panel'>
            <input type='file' id='fileSelector' value='' onChange={this.fileSelected} />
            <label htmlFor='fileSelector'>
                <span className='pt-button pt-icon-upload pt-intent-success'>Dodaj plik</span>
            </label>
        </div>;
    }


    render() {
        const { newFile, files, loadingFiles } = this.state;
        const { courseId } = this.props;
        return (
            <div className="files-container">
                {this.renderAddButton()}
                <FilesTable data={files} loading={loadingFiles} />
                <MobileFilesTable data={files} loading={loadingFiles} />
                {newFile ? <FileDialog file={newFile} onClose={this.resetFile} courseId={courseId} fetchFiles={this.getCourseFiles} /> : ''}
            </div>
        );
    }
}
