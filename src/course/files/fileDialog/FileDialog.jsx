import _ from 'lodash';
import React, { Component } from 'react';
import { Dialog, Button, Intent, ProgressBar, NonIdealState } from '@blueprintjs/core';
import validator from 'validator';
import FILE_TYPES from './../FileTypes';
import courseFileUpload from './../courseFileUpload';

export default class FileDialog extends Component {
    constructor(props) {
        super(props);
        this.state = DEFAULT_STATE;

        this.addFile = this.addFile.bind(this);
        this.descriptionChange = this.descriptionChange.bind(this);
        this.fileTypeChange = this.fileTypeChange.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.onUploadFinish = this.onUploadFinish.bind(this);
        this.onUploadFailed = this.onUploadFailed.bind(this);
        this.uploadControl = {
            task: { cancel: () => { } },
            onStateChange: this.fileUploadStateChanged.bind(this)
        };
    }

    descriptionChange(event) { this.setState({ description: event.target.value }) }
    fileTypeChange(event) { this.setState({ fileType: event.target.value }) }
    fileUploadStateChanged(state, bytesTransferred, totalBytes) { this.setState({ uploading: { state, bytesTransferred, totalBytes } }) }

    closeDialog() {
        this.uploadControl.task.cancel();
        this.props.onClose();
    }

    validate() {
        const { description } = this.state;
        let ok = true;
        if (!validator.isLength(description, { min: 0, max: 250 })) {
            this.setState({ error: { field: 'description', info: 'Opis nie powinien być dłuższy niż 250 znaków' } });
            ok = false;
        }
        return ok;
    }

    onUploadFinish() {
        this.setState({savingState: SAVING_FINISHED});
        this.props.fetchFiles();
    }

    onUploadFailed() {
        this.setState({savingState: SAVING_FAILED});
    }

    addFile() {
        if (this.validate()) {
            const { file, courseId } = this.props;
            courseFileUpload(courseId, file, this.state, this.uploadControl, this.onUploadFinish)
                .catch(error => {
                    this.setState({ error: ERRORS_MAP[error.message] });
                });
        }
    }

    renderFileTypeSelect() {
        const { fileType } = this.state;
        let filesTypesOptions = [];
        _.each(FILE_TYPES, (o) => { filesTypesOptions.push(<option key={o.code} value={o.code} label={o.name} />) });

        return <div className='pt-select  pt-fill'>
            <select value={fileType} onChange={this.fileTypeChange}>
                {filesTypesOptions}
            </select>
        </div>;
    }

    renderBody() {
        const { description, error } = this.state;
        const { file } = this.props;
        return <div className='pt-dialog-body file-details'>
            <label className='pt-label'>
                Nazwa pliku: <span className='filename'>{file.name}</span>
            </label>
            <label className='pt-label'> Opis:
                <textarea className={'pt-input pt-fill' + (error.field === 'description' ? ' pt-intent-danger' : '')}
                    dir='auto' value={description} onChange={this.descriptionChange} />
            </label>
            {this.renderFileTypeSelect()}
            {error.field ? <div className="pt-callout pt-intent-danger">{error.info}</div> : ''}
        </div>;
    }

    renderUploadProgress() {
        const { savingState } = this.state;
        const { bytesTransferred, totalBytes } = this.state.uploading;
        let progress = bytesTransferred / totalBytes;
        if (progress === 1 && savingState === SAVING_FINISHED) {
            return <NonIdealState title='Pomyślnie dodano plik' visual='tick' />;
        } else if(savingState === SAVING_FINISHED) {
            return <NonIdealState title='Podczas dodwania pliku wystąpił błąd' visual='cross' />;
        }
        return <div className='pt-dialog-body file-details'>
            <ProgressBar value={progress} />
            {_.round(progress * 100, 1) + '%'}
        </div>;
    }

    renderFooter() {
        const { state } = this.state.uploading;
        return <div className='pt-dialog-footer'>
            <div className='pt-dialog-footer-actions'>
                <Button text='Anuluj' onClick={this.closeDialog} />
                {state ? '' : <Button iconName='floppy-disk' intent={Intent.SUCCESS} onClick={this.addFile} className='default-button' text='Zapisz' />}
            </div>
        </div>;
    }

    render() {
        const { file } = this.props;
        const { uploading } = this.state;
        return (
            <Dialog isOpen={file} onClose={this.closeDialog} title='Dodaj plik' className='file-dialog'>
                {uploading.state ? this.renderUploadProgress() : this.renderBody()}
                {this.renderFooter()}
            </Dialog>
        );
    }
}

const DEFAULT_STATE = {
    description: '',
    fileType: FILE_TYPES[0].code,
    error: {},
    uploading: {
        state: null,
        bytesTransferred: 0,
        totalBytes: 1
    },
    savingState: null
};

const ERRORS_MAP = {
    FILE_ALREADY_EXISTS: { field: 'FILE_ALREADY_EXISTS', info: 'Plik o tej nazwie już istnieje' },
    ERROR: { field: 'ERROR', info: 'Wystąpił błąd w trakcie wysyłania pliku' }
}

const SAVING_FINISHED = 'FINISHED';
const SAVING_FAILED = 'FAILED';