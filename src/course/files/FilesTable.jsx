import _ from 'lodash';
import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { NonIdealState } from '@blueprintjs/core';
import FILE_TYPES from './FileTypes';
import downloadFile from './downloadFile';

function timeFormatter(cell, row) {
    let date = new Date(cell);
    return <span title={date.toLocaleDateString() + ' ' + date.toLocaleTimeString()}>{date.toLocaleDateString()}</span>;
}

function typeFormatter(cell, row) {
    return _.find(FILE_TYPES, { code: cell }).name;
}

export default class FilesTable extends Component {

    static get TABLE_EMPTY() {
        return <NonIdealState title='Brak plików' description='Nikt nie dodał jeszcze żadnego pliku, bądź pierwszy!' visual='inbox' />;
    }

    static get TABLE_OPTIONS() {
        return { noDataText: FilesTable.TABLE_EMPTY, onRowClick: downloadFile };
    }

    render() {
        return (
            <BootstrapTable data={this.props.data} options={FilesTable.TABLE_OPTIONS} className='files-table'
                tableHeaderClass='files-table-header' tableBodyClass='files-table-body' hover pagination>
                <TableHeaderColumn isKey dataField='name'>Nazwa</TableHeaderColumn>
                <TableHeaderColumn dataField='description'>Opis</TableHeaderColumn>
                <TableHeaderColumn dataField='fileType' dataFormat={typeFormatter}>Kategoria</TableHeaderColumn>
                <TableHeaderColumn dataField='displayName'>Autor</TableHeaderColumn>
                <TableHeaderColumn dataField='timestamp' dataFormat={timeFormatter}>Data dodatnia</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}
