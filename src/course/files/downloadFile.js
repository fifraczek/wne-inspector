import * as firebase from 'firebase';
import download from 'downloadjs';
import { Intent } from '@blueprintjs/core';
import { Toast } from '../../components/toast/Toast';


const downloadFile = (row) => {
    let pathReference = firebase.storage().ref(row.path);
    Toast.show({ message: 'Twoje pobieranie wkrótce się rozpocznie', intent: Intent.PRIMARY });
    pathReference.getDownloadURL().then((url) => {
        let xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function (event) {
            let blob = xhr.response;
            download(blob, row.name);
        };
        xhr.open('GET', url);
        xhr.send();
    }).catch((error) => {
        Toast.show({ message: 'W przy pobieraniu pliku wystąpił błąd', intent: Intent.DANGER });
    });
};

export default downloadFile;