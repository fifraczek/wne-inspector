import _ from 'lodash';
import fileUpload from './../../global/utils/fileUpload';
import runFunction from './../../global/utils/runFunction';

const courseFileUpload = (courseId, fileObj, fileDetails, uploadControl, onUploadFinish, onUploadFailed) => {
    const path = 'course/' + courseId + '/' + fileObj.name;
    const onSuccess = () => {
        let data = _.assign({ path, name: fileObj.name, courseId }, fileDetails );
        delete data.uploading;
        delete data.error;
        runFunction('addFile', data, (json) => {
            if (json.ok) {
                onUploadFinish();
            } else {
                onUploadFailed();
            }
        });
    };
    return fileUpload(fileObj, path, uploadControl, onSuccess);
};

export default courseFileUpload;