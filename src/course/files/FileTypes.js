const FILE_TYPES = [
    {code: 'W', name: 'Wykład'},
    {code: 'C', name: 'Ćwiczenia'},
    {code: 'K', name: 'Konwersatorium'},
    {code: 'P', name: 'Projekt'},
    {code: 'E', name: 'Egzamin'},
    {code: 'I', name: 'Inne'}
];

export default FILE_TYPES;