import React, { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import FilesTable from './../FilesTable';
import FileDetails from './FileDetails';


export default class MobileFilesTable extends Component {

    expandComponent(row) {
        return <FileDetails data={row} />;
    }

    render() {
        const TABLE_OPTIONS = {
            noDataText: FilesTable.TABLE_EMPTY, 
            expandRowBgColor: 'rgb(240, 240, 240)'
        }
        return (
            <BootstrapTable data={this.props.data} options={TABLE_OPTIONS} className='mobile-files-table'
                expandComponent={this.expandComponent} expandableRow={() => true} ref={(table) => { this.tableRef = table; }}
                tableHeaderClass='files-table-header' tableBodyClass='files-table-body' hover pagination>
                <TableHeaderColumn isKey dataField='name'>Nazwa</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}
