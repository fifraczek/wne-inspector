import _ from 'lodash';
import React, { Component } from 'react';
import FILE_TYPES from './../FileTypes';
import { Icon } from '@blueprintjs/core';
import downloadFile from './../downloadFile';


export default class FileDetails extends Component {
    render() {
        const { description, displayName, fileType, timestamp } = this.props.data;
        return (
            <div className='file-details'>
                <div className='file-details-data'>
                    {description === '' ? '' : <div><b>Opis: </b>{description}</div>}
                    <div><b>Autor: </b>{displayName}<br /></div>
                    <div><b>Kategoria: </b>{_.find(FILE_TYPES, { code: fileType }).name}<br /></div>
                    <div><b>Dodano: </b>{new Date(timestamp).toLocaleDateString()}</div>
                </div>
                <div className='file-details-download'>
                    <Icon iconName='download' onClick={()=>downloadFile(this.props.data)}/>
                </div>
            </div>
        );
    }
}
