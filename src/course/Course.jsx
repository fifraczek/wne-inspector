import React, { Component } from 'react';
import * as firebase from 'firebase';
import { Button, Intent, Tab2, Tabs2, Icon } from '@blueprintjs/core';
import Rates from './rates/Rates';
import Comments from './comments/Comments';
import Files from './files/Files';
import Loading from '../components/loading/Loading';
import './course.css';

export default class Course extends Component {
    constructor(props) {
        super(props);
        this.state = {
            courseId: props.match.params.code,
            courseName: '',
            loadingName: true
        }
    }

    toggleAddRateDialog() {
        this.setState({ addRateOpen: !this.state.addRateOpen });
    }

    componentWillMount() {        
        firebase.database().ref('/coursesMap/' + this.state.courseId).once('value').then(data => this.setState({ courseName: data.val(), loadingName: false }));
    }

    renderTitle() {
        const { courseName, courseId } = this.state;
        return <div className='title'>
            <span>{courseName}</span>
            <a target='_blank' href={'https://usosweb.wne.uw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=' + courseId}>
                <Button iconName='share' intent={Intent.WARNING}>
                    <span className='add-button-text'>Strona w systemie USOS</span>
                </Button></a>
        </div>;
    }

    render() {
        const { loadingName, courseId } = this.state;
        if (loadingName) {
            return <Loading />;
        }
        const commentTabTitle = <span> <Icon iconName='comment' /> Dyskusja</span>
        const ratesTabTitle = <span> <Icon iconName='star' /> Opinie</span>
        const filesTabTitle = <span> <Icon iconName='folder-open' /> Pliki</span>

        return (
            <div className='course-page page-container'>
                {this.renderTitle()}
                <Tabs2 className='main-tabs'>
                    <Tab2 id='rates' title={ratesTabTitle} panel={<Rates courseId={courseId} />} />
                    <Tab2 id='comments' title={commentTabTitle} panel={<Comments/>} />
                    <Tab2 id='files' title={filesTabTitle} panel={<Files courseId={courseId} />} />
                </Tabs2>
            </div>
        );
    }
}
