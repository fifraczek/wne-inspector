import React from 'react';

const STARS = {
    difficulty: {
        1: 'bardzo łatwy',
        2: 'łatwy',
        3: 'średni',
        4: 'trudny',
        5: 'bardzo trudny'
    },
    teachersApproach: {
        1: 'bardzo negatywne',
        2: 'negatywne',
        3: 'neutralne',
        4: 'pozytywne',
        5: 'bardzo pozytywne'
    },
    materials: {
        1: 'bardzo niska',
        2: 'niska',
        3: 'przeciętna',
        4: 'dobra',
        5: 'bardzo dobra'
    },
    interesting: {
        1: 'zupełnie nieciekawa',
        2: 'mało ciekawa',
        3: 'przeciętna',
        4: 'ciekawa',
        5: 'bardzo ciekawa'
    }
};

const getHint = function(name, value) {
    return <div className='rate-hint'>{STARS[name][value]}</div>;
};



export default getHint;
