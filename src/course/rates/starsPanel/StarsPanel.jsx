import React, { Component } from 'react';
import { Icon } from '@blueprintjs/core';
import './stars-panel.css';

export default class StarsPanel extends Component {

    renderStars() {
        const { param, changeHandler } = this.props;
        let stars = [];
        for (let i = 1; i < 6; i++) {
            stars.push(<Icon key={i} iconName={param >= i ? 'star' : 'star-empty'} iconSize={Icon.SIZE_LARGE} onClick={() => changeHandler(i)} />);
        }
        return <div className='stars-container'>
            {stars}
        </div>
    }

    render() {
        const { title } = this.props;
        return (
            <div className='stars-panel'>
                <div className='title'>{title}</div>
                {this.renderStars()}
            </div>
        );
    }
}