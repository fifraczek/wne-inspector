import './stats.css';

import React, { Component } from 'react';
import { Icon } from '@blueprintjs/core';
import { Row, Col } from 'react-bootstrap';
import getHint from '../Stars';

export default class Stats extends Component {

    renderNStars(n) {
        let stars = []
        let rounded = Math.round(n);
        for (let i = 0; i < rounded; i++) {
            stars.push(<Icon key={i} iconName={'star'} className='pt-icon-large' />)
        }
        return <div className='stats-icons'> {stars} </div>
    }

    render() {
        const { difficulty, interesting, materials, teachersApproach } = this.props;
        return (
            <div className='stats pt-fill'>
                <div className='stats-content'>
                    <Row>
                        <Col xs={6} md={3}>
                            <div className='stats-entity'>
                                <div className='stats-subtitle'>Poziom trudności:</div>
                                {this.renderNStars(difficulty)}
                                <div className='stats-number'>{formatNumber(difficulty)}</div>
                                {getHint('difficulty',  Math.round(difficulty))}
                            </div>
                        </Col>
                        <Col xs={6} md={3}>
                            <div className='stats-entity'>
                                <div className='stats-subtitle'>Podejście prowadzącego:</div>
                                {this.renderNStars(teachersApproach)}
                                <div className='stats-number'>{formatNumber(teachersApproach)}</div>
                                {getHint('teachersApproach',  Math.round(teachersApproach))}
                            </div>
                        </Col>
                        <Col xs={6} md={3}>
                            <div className='stats-entity'>
                                <div className='stats-subtitle'>Dostępność materiałów:</div>
                                {this.renderNStars(materials)}
                                <div className='stats-number'>{formatNumber(materials)}</div>
                                {getHint('materials', Math.round(materials))}
                            </div>
                        </Col>
                        <Col xs={6} md={3}>
                            <div className='stats-entity'>
                                <div className='stats-subtitle'>Tematyka:</div>
                                {this.renderNStars(interesting)}
                                <div className='stats-number'>{formatNumber(interesting)}</div>
                                {getHint('interesting', Math.round(interesting))}
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const formatNumber = (number) => {
    if (isNaN(number)) {
        return '';
    }
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}