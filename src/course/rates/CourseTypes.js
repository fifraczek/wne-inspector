const COURSE_TYPES = [
    {code: 'W', name: 'Wykład'},
    {code: 'C', name: 'Ćwiczenia'},
    {code: 'K', name: 'Konwersatorium'}
];

export default COURSE_TYPES;