import _ from 'lodash';
import React, { Component } from 'react';
import { Dialog, Button, Intent } from '@blueprintjs/core';
import { Row, Col } from 'react-bootstrap';
import validator from 'validator';
import StarsPanel from '../starsPanel/StarsPanel';
import COURSE_TYPES from '../CourseTypes';
import getHint from '../Stars';
import runFunction from '../../../global/utils/runFunction';
import { Toast } from '../../../components/toast/Toast';
import './rate-dialog.css';

export default class RateDialog extends Component {
    constructor(props) {
        super(props);
        this.state = DEFAULT_STATE;
    }

    difficultyChange(newValue) { this.setState({ difficulty: newValue }) }
    materialsChange(newValue) { this.setState({ materials: newValue }) }
    teachersApproachChange(newValue) { this.setState({ teachersApproach: newValue }) }
    interestingChange(newValue) { this.setState({ interesting: newValue }) }
    courseTypeChange(event) { this.setState({ courseType: event.target.value }) }
    teacherChange(event) { this.setState({ teacher: event.target.value }) }
    commentChange(event) { this.setState({ comment: event.target.value }) }


    validate() {
        const { teacher, comment } = this.state;
        let ok = true;
        if (!validator.isLength(teacher, { min: 0, max: 100 })) {
            this.setState({ error: { field: 'teacher', info: 'Pole "Prowadzący" powinno zawierać nie więcej niż 100 znaków' } });
            ok = false;
        } else if (!validator.isLength(comment, { min: 0, max: 2500 })) {
            this.setState({ error: { field: 'comment', info: 'Pole "Komentarz" powinno zawierać nie więcej niż 2500 znaków' } });
            ok = false;
        }
        return ok;
    }

    addRate() {
        if (this.validate()) {
            let data = _.assign({}, this.state);
            data.courseId = this.props.courseId;
            delete data.error;
            runFunction('addRate', data, (json) => {
                if (json.ok) {
                    this.props.onSuccess();
                    Toast.show({ message: 'Pomyślnie dodano opinię.', intent: Intent.SUCCESS });
                    this.setState(DEFAULT_STATE);
                } else {
                    Toast.show({ message: ERRORS_MAP[json.error], intent: Intent.DANGER });
                }
            });
            this.props.onClose();
        }
    }

    renderStarsPanels() {
        const { difficulty, materials, teachersApproach, interesting } = this.state;
        return <div>
            <Row>
                <Col xs={12} md={6}>
                    <StarsPanel title='Poziom trudności' param={difficulty} changeHandler={this.difficultyChange.bind(this)} />
                    {getHint('difficulty', difficulty)}
                </Col>
                <Col xs={12} md={6}>
                    <StarsPanel title='Dostępność materiałów' param={materials} changeHandler={this.materialsChange.bind(this)} />
                    {getHint('materials', materials)}
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={6}>
                    <StarsPanel title='Podejście prowadzącego' param={teachersApproach} changeHandler={this.teachersApproachChange.bind(this)} />
                    {getHint('teachersApproach', teachersApproach)}
                </Col>
                <Col xs={12} md={6}>
                    <StarsPanel title='Tematyka' param={interesting} changeHandler={this.interestingChange.bind(this)} />
                    {getHint('interesting', interesting)}
                </Col>
            </Row>
        </div>
    }

    renderDetailsPanel() {
        const { courseType, teacher, comment, error } = this.state;
        let coursesTypesOptions = [];
        _.each(COURSE_TYPES, (o) => { coursesTypesOptions.push(<option key={o.code} value={o.code} label={o.name} />) });
        return <div>
            <Row>
                <Col xs={12} md={6}>
                    <label className='pt-label'>Rodzaj zajęć:
                        <div className='pt-select  pt-fill'>
                            <select value={courseType} onChange={this.courseTypeChange.bind(this)}>
                                {coursesTypesOptions}
                            </select>
                        </div>
                    </label>
                </Col>
                <Col xs={12} md={6}>
                    <label className='pt-label'>
                        Prowadzący:
                    <input className={'pt-input pt-fill' + (error.field === 'teacher' ? ' pt-intent-danger' : '')}
                            type='text' dir='auto' value={teacher} onChange={this.teacherChange.bind(this)} />
                    </label>
                </Col>
            </Row>
            <Row>
                <Col xs={12} md={12}>
                    <label className='pt-label'>
                        Komentarz:
                        <textarea className={'pt-input pt-fill' + (error.field === 'comment' ? ' pt-intent-danger' : '')}
                            dir='auto' value={comment} onChange={this.commentChange.bind(this)} />
                    </label>
                </Col>
            </Row>
        </div>
    }

    renderFooter() {
        return <div className='pt-dialog-footer'>
            <div className='pt-dialog-footer-actions'>
                <Button text='Anuluj' onClick={this.props.onClose} />
                <Button iconName='tick' intent={Intent.SUCCESS} onClick={this.addRate.bind(this)} className='default-button' text='Zapisz' />
            </div>
        </div>;
    }

    render() {
        const { error } = this.state;
        return (
            <Dialog isOpen={this.props.open} onClose={this.props.onClose} title='Dodaj opinię' className='rate-dialog'>
                <div className='pt-dialog-body'>
                    {this.renderStarsPanels()}
                    {this.renderDetailsPanel()}
                    {error.field ? <div className="pt-callout pt-intent-danger">{error.info}</div> : ''}
                </div>
                {this.renderFooter()}
            </Dialog>
        );
    }
}

const DEFAULT_STATE = {
    difficulty: 3,
    materials: 3,
    teachersApproach: 3,
    interesting: 3,
    courseType: 'W',
    teacher: '',
    comment: '',
    error: {}
};

const ERRORS_MAP = {
    VALIDATION_FAILED: 'Błąd walidacji'
}
