import _ from 'lodash';
import React, { Component } from 'react';
import fetchDatabase from '../../global/utils/fetchDatabase';
import { Button, Intent } from '@blueprintjs/core';
import RateDialog from './rateDialog/RateDialog';
import RatePanel from './ratePanel/RatePanel';
import Stats from './stats/Stats';
import Loading from '../../components/loading/Loading';

export default class Rates extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addRateOpen: false,
            rates: [],
            stats: {},
            loadingRates: true
        }
    }

    toggleAddRateDialog() {
        this.setState({ addRateOpen: !this.state.addRateOpen });
    }

    componentDidMount() {
        this.getCourseRates();
    }

    getCourseRates() {
        this.setState({ loadingRates: true });
        fetchDatabase('course/' + this.props.courseId + '/rates', (result) => {
            let resultArray = _.transform(result, (output, value, key) => {
                output.push(_.assign({},value, {id: key, courseId: this.props.courseId}));
              }, []);
              resultArray = _.reverse(_.sortBy(resultArray, ['timestamp']));
            this.setState({ rates: resultArray, stats: calcStats(resultArray), loadingRates: false });
        }
        );
    }

    renderAddButton() {
        return <Button iconName='edit' className='pt-fill' intent={Intent.SUCCESS} onClick={this.toggleAddRateDialog.bind(this)}>
            <span className='add-button-text'>Dodaj opinię</span>
        </Button>;
    }

    renderRates() {
        let ratePanels = [];
        this.state.rates.forEach((rate, i) => ratePanels.push(<RatePanel key={'ratePanel' + i} data={rate} />));
        return <div className='rate-container'>
            {ratePanels}
        </div>;
    }


    render() {
        const { addRateOpen, stats, rates, loadingRates } = this.state;
        const statsPanel = <Stats difficulty={stats.difficulty} teachersApproach={stats.teachersApproach} materials={stats.materials} interesting={stats.interesting} />;
        const noRates = <div className="pt-callout pt-intent-warning">
            <h4>Brak opinii</h4>
            Ten przedmiot nie został jeszcze oceniony. Bądź pierwszy!
        </div>;
        if (loadingRates) {
            return <Loading />;
        }
        return (
            <div className="rates-container">
                {rates.length < 1 ? noRates : statsPanel}
                {this.renderAddButton()}
                <br />
                {this.renderRates()}
                <RateDialog open={addRateOpen}
                    onClose={this.toggleAddRateDialog.bind(this)}
                    courseId={this.props.courseId}
                    onSuccess={this.getCourseRates.bind(this)} />
            </div>
        );
    }
}

const calcStats = (data) => {
    let difficultySum = 0;
    let teachersApproachSum = 0;
    let materialsSum = 0;
    let interestingSum = 0;
    data.forEach((o) => {
        difficultySum += o.difficulty;
        teachersApproachSum += o.teachersApproach;
        materialsSum += o.materials;
        interestingSum += o.interesting;
    });
    let length = data.length;
    return {
        difficulty: difficultySum / length,
        teachersApproach: teachersApproachSum / length,
        materials: materialsSum / length,
        interesting: interestingSum / length
    };
}