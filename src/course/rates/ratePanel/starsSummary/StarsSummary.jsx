import './stars-summary.css';

import React, { Component } from 'react';
import { Icon } from '@blueprintjs/core';
import { Row, Col } from 'react-bootstrap';
import getHint from '../../Stars';

export default class StarsSummary extends Component {

    renderNStars(n) {
        let stars = []
        for (let i = 0; i < n; i++) {
            stars.push(<Icon key={i} iconName={'star'} />)
        }
        return <div className='stars-summary-icons'> {stars} </div>
    }

    render() {
        const { difficulty, interesting, materials, teachersApproach } = this.props;
        return (
            <div className='stars-summary pt-fill'>
                <Row>
                    <Col xs={6} md={3}>
                        <div className='stars-summary-entity'>
                            <div className='stars-summary-title'>Poziom trudności:</div>
                            {this.renderNStars(difficulty)}
                            {getHint('difficulty', Math.round(difficulty))}
                        </div>
                    </Col>
                    <Col xs={6} md={3}>
                        <div className='stars-summary-entity'>
                            <div className='stars-summary-title'>Podejście prowadzącego:</div>
                            {this.renderNStars(teachersApproach)}
                            {getHint('teachersApproach', Math.round(teachersApproach))}
                        </div>
                    </Col>
                    <Col xs={6} md={3}>
                        <div className='stars-summary-entity'>
                            <div className='stars-summary-title'>Dostępność materiałów:</div>
                            {this.renderNStars(materials)}
                            {getHint('materials', Math.round(materials))}
                        </div>
                    </Col>
                    <Col xs={6} md={3}>
                        <div className='stars-summary-entity'>
                            <div className='stars-summary-title'>Tematyka:</div>
                            {this.renderNStars(interesting)}
                            {getHint('interesting', Math.round(interesting))}
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}