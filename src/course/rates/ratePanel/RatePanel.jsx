import './rate-panel.css';

import _ from 'lodash';
import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import { Icon } from '@blueprintjs/core';
import StarsSummary from './starsSummary/StarsSummary';
import COURSE_TYPES from '../CourseTypes';


export default class RatePanel extends Component {

    render() {
        const { comment, courseType, difficulty, displayName, interesting, materials, teacher, teachersApproach, timestamp } = this.props.data;
        const reportIcon = <a href={reportMailHref(this.props.data)}><Icon iconName="warning-sign" /></a>
        const date = new Date(timestamp)
        const dateString = date.toLocaleDateString() + ' ' + date.toLocaleTimeString()

        return (
            <div className='rate-panel pt-fill'>
                <Row>
                    <Col xs={12} md={3}>
                        <div className='main-card'>
                            <div className='course-type'>{_.find(COURSE_TYPES, { code: courseType }).name}</div>
                            <div className='teacher'>Prowadzący:</div>
                            <div className='teacher-name'>{teacher}</div>
                            {}
                        </div>
                    </Col>
                    <Col xs={12} md={9}>
                        <div className='details-card'>
                            <div className='header'>{displayName}, {dateString} {reportIcon}</div>
                            <StarsSummary difficulty={difficulty} interesting={interesting} materials={materials} teachersApproach={teachersApproach} />
                            <div className='content'>{comment}</div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const reportMailHref = (data) => {
    let href = 'mailto:wne.inspector@gmail.com?subject=Zgłoszenie naruszenia regulaminu&body='
    let body = 'Zgłoszenie dotyczące przedmiotu:' + data.courseId + ' , wypowiedź: ' + data.id + '%0A%0APoniżej proszę podać uzasadnienie:%0A';
    return href + body;
};
