import './add-course-dialog.css';

import React, { Component } from 'react';
import { Button, Dialog, Intent } from '@blueprintjs/core';
import { Toast } from '../../components/toast/Toast';
import runFunction from '../../global/utils/runFunction';
import addTooltip from '../../resources/images/add-tooltip.PNG'

const ERRORS_MAP = { ALREADY_EXIST: 'Przedmiot został dodany wcześniej', NOT_FOUND: 'Nie znaleziono przedmiotu' };


export default class AddCourseDialog extends Component {
  constructor(props) {
    super(props);
    this.state = { courseId: '', infoVisible: false }
  }

  onCourseIdChange(event) { this.setState({ courseId: event.target.value }); }

  addCourse() {
    runFunction('addCourse', { id: this.state.courseId.trim() }, (json) => {
      if (json.ok) {
        this.props.onSuccess();
        Toast.show({ message: 'Pomyślnie dodano przedmiot', intent: Intent.SUCCESS });
        this.setState({ courseId: '' });
      } else {
        Toast.show({ message: ERRORS_MAP[json.error], intent: Intent.DANGER });
      }
    });
    this.props.onClose();
  }

  toggleInfo() {
    this.setState({ infoVisible: !this.state.infoVisible })
  }

  renderForm() {
    const { courseId } = this.state;
    return <div className='pt-dialog-body'>
      <div className='information-text'>Wprowadź kod przedmiotu z systemu USOS</div>
      <div className='pt-input-group'>
        <input type='text' className='pt-input' placeholder='Kod przedmiotu' value={courseId} onChange={this.onCourseIdChange.bind(this)} />
        <span className='pt-icon pt-icon-tag' />
      </div>
    </div>;
  }

  renderFooter() {
    return <div className="pt-dialog-footer">
      <div className="pt-dialog-footer-actions">
        <Button iconName="help" className='pt-minimal pt-small' intent={Intent.PRIMARY} text='Jaki kod?' onClick={this.toggleInfo.bind(this)} />
        <Button className='default-button' onClick={this.addCourse.bind(this)} text="Zapisz" />
      </div>
    </div>;
  }

  render() {
    let image = this.state.infoVisible ? <div className="add-tooltip"><img src={addTooltip} alt="add" /></div> : '';
    return (
      <Dialog isOpen={this.props.open} onClose={this.props.onClose} title='Dodaj przedmiot' className='add-course-dialog'>
        {this.renderForm()}
        {this.renderFooter()}
        {image}
      </Dialog>
    );
  }
}
