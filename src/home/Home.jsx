import './home.css';

import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import fetchDatabase from '../global/utils/fetchDatabase';
import { Button, Intent } from '@blueprintjs/core';
import AddCourseDialog from './addCourseDialog/AddCourseDialog';
import Loading from '../components/loading/Loading';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      searchText: '',
      courses: {},
      addCourseOpen: false
    }
  }

  componentWillMount() {
    this.getCoursesList();
  }

  onSearchTextChange(event) {
    if (event.target.value === 'parostatek') {
      window.open('https://www.youtube.com/watch?v=f8gaMlsP-xs', '_blank');
    }
    this.setState({ searchText: event.target.value });
  }

  toggleDialog() {
    this.setState({ addCourseOpen: !this.state.addCourseOpen });
  }

  getCoursesList() {
    this.setState({ loading: true });
    let objectToArray = object => _.transform(object, (result, value, key) =>
      result.push({ id: key, name: value })
      , []);
    fetchDatabase('coursesMap', result => this.setState({ courses: objectToArray(result), loading: false }));
  }

  clearSearch() {
    this.setState({ searchText: '' });
  }

  renderTitle() {
    return <div className='title'>
      <span>Wyszukaj przedmiot</span>
      <Button iconName='add' intent={Intent.SUCCESS} onClick={this.toggleDialog.bind(this)}>
        <span className='add-button-text'>Dodaj nowy</span>
      </Button>
    </div>;
  }

  renderSearch() {
    let icon = this.state.searchText === '' ? 'pt-icon-search pt-intent-primary' : 'pt-icon-cross pt-intent-danger';
    return <div className='pt-input-group pt-large search-bar'>
      <input type='text' className='pt-input' placeholder='Wpisz nazwę przedmiotu'
        value={this.state.searchText} onChange={this.onSearchTextChange.bind(this)} />
      <button className={'pt-button pt-minimal ' + icon} onClick={this.clearSearch.bind(this)} />
    </div>;
  }

  renderCoursesList() {
    const { courses, searchText } = this.state;
    let filtered = _.filter(courses, o =>
      (_.includes(o.name.toUpperCase(), searchText.toUpperCase()) || _.includes(o.id.toUpperCase(), searchText.toUpperCase())));
    let sorted = _.sortBy(filtered, o => o.name);
    return <div className='pt-button-group pt-vertical pt-fill pt-align-left'>
      {_.map(sorted, o => renderCourseButton(o))}
    </div>
  }

  render() {
    return (
      <div className='home-page page-container'>
        {this.renderTitle()}
        {this.renderSearch()}
        {this.state.loading ? <Loading /> : this.renderCoursesList()}
        <AddCourseDialog open={this.state.addCourseOpen} onClose={this.toggleDialog.bind(this)} onSuccess={this.getCoursesList.bind(this)} />
      </div>
    );
  }
}

const renderCourseButton = (data) => {
  return <Link to={'/course/' + data.id} key={data.id}>
    <button type='button' className='pt-button course-button pt-fill'>
      <span className='course-name'>{data.name}</span>
      <span className='course-id-subtext'>{data.id}</span>
    </button>
  </Link>
}
