import * as firebase from "firebase";
import React, { Component } from 'react';
import validator from 'validator';
import { Popover, Position, Intent } from '@blueprintjs/core';
import { Toast } from '../../components/toast/Toast';

export default class ForgottenPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            error: { field: '', message: '' }
        }
    }

    onEmailChange(event) { this.setState({ email: event.target.value }); }

    resetPassword(event) {
        event.preventDefault();
        const { email } = this.state;
        let ok = true;
        if (!validator.isEmail(email)) {
            this.setState({ error: { field: 'email', info: 'Niepoprawny adres e-mail.' } });
            ok = false;
        }
        if (ok) {
            firebase.auth().sendPasswordResetEmail(email).then(() => {
                Toast.show({ message: 'Link do zmiany hasła został wysłany na podany adres e-mail', intent: Intent.SUCCESS });
            }).catch((error) =>{
                console.error(error.message);
            });
        }

    }

    renderTarget() {
        return <div className='register-footer'>
            <span>Zapomniałem hasła</span>
        </div>;
    }

    renderContent() {
        const { error, email } = this.state;
        return <div>
            <h5>Wpisz adres e-mail podany przy rejestracji:</h5>
            <form className='login-page-form' onSubmit={this.resetPassword.bind(this)}>
            <div className="pt-input-group ">
                <input className={'pt-input' + (error.field === 'password' ? ' pt-intent-danger' : '')}
                    type='text' placeholder='Adres e-mail' value={email} onChange={this.onEmailChange.bind(this)} />
                <button className="pt-popover-dismiss pt-button pt-minimal pt-intent-primary pt-icon-arrow-right" type="submit"/>
            </div>
            {error.field ? <div className="pt-callout pt-intent-danger">{error.info}</div> : ''}
            </form>
        </div>;
    }

    render() {
        return (
            <Popover
                popoverClassName="pt-popover-content-sizing"
                position={Position.TOP}
                content={this.renderContent()}
                target={this.renderTarget()} />
        );
    }
}
