import './login.css';

import * as firebase from "firebase";
import React, { Component } from 'react';
import { Intent } from '@blueprintjs/core';
import RegisterDialog from './registerDialog/RegisterDialog';
import ForgottenPassword from './forgottenPassword/ForgottenPassword';
import { Toast } from '../components/toast/Toast';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      registerOpen: false,
      error: { field: '' }
    }
  }

  onEmailChange(event) { this.setState({ email: event.target.value }); }
  onPasswordChange(event) { this.setState({ password: event.target.value }); }

  login(event) {
    event.preventDefault();
    this.setState({ error: { field: '' } });
    firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((result) => { })
      .catch((error) => {
        var errorCode = error.code;
        if (errorCode === 'auth/wrong-password') {
          this.setState({ error: { field: 'password' } });
          Toast.show({ message: 'Niepoprawne hasło', intent: Intent.DANGER });
        }
        if (errorCode === 'auth/invalid-email') {
          this.setState({ error: { field: 'email' } });
          Toast.show({ message: 'Niepoprawny adres e-mail', intent: Intent.DANGER });
        }
        if (errorCode === 'auth/user-not-found') {
          this.setState({ error: { field: 'email' } });
          Toast.show({ message: 'Użytkownik o podanym adresie nie istnieje', intent: Intent.DANGER });
        }
        console.error(error.message);
      });
  }

  toggleDialog() {
    this.setState({ registerOpen: !this.state.registerOpen });
  }

  renderLoginTitle() {
    return <div className='login-title'>
      <div className='primary'>WNE</div>
      <div className='secondary'>Inspector</div>
    </div>;
  }

  renderLoginForm() {
    const { email, password, error } = this.state;
    return <form className='login-page-form' onSubmit={this.login.bind(this)}>
      <input className={'pt-input pt-fill' + (error.field === 'email' ? ' pt-intent-danger' : '')}
        type='text' placeholder='Adres e-mail' value={email} onChange={this.onEmailChange.bind(this)} name="username" />
      <input className={'pt-input pt-fill' + (error.field === 'password' ? ' pt-intent-danger' : '')}
        type='password' placeholder='Hasło' value={password} onChange={this.onPasswordChange.bind(this)} name="password" />
      <input className='pt-button login-button' type="submit" value="Zaloguj się" />
      <div className='register-footer'>Nie masz konta? <span onClick={this.toggleDialog.bind(this)}>Zarejestruj się!</span></div>
      <ForgottenPassword />
    </form>;
  }

  render() {
    return (
      <div className='login-page-container'>
        <div className='login-page-content'>
          {this.renderLoginTitle()}
          {this.renderLoginForm()}
        </div>
        <RegisterDialog open={this.state.registerOpen} onClose={this.toggleDialog.bind(this)} />
      </div>
    );
  }
}
