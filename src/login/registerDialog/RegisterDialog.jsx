import './register-dialog.css';

import _ from 'lodash';
import * as firebase from "firebase";
import React, { Component } from 'react';
import { Button, Dialog, Checkbox, Spinner } from '@blueprintjs/core';
import validator from 'validator';
import runFunctionAnonymusly from '../../global/utils/runFunctionAnonymusly';
import runFunction from '../../global/utils/runFunction';
import terms from '../../resources/pdfs/regulamin.pdf'


export default class RegisterDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      error: { field: '', info: '' },
      loginAvailable: false,
      termsAccepted: false,
      checkingUsername: false
    }
  }

  onUsernameChange(event) { this.setState({ username: event.target.value, loginAvailable: false }); }
  onEmailChange(event) { this.setState({ email: event.target.value }); }
  onPasswordChange(event) { this.setState({ password: event.target.value }); }
  onPasswordConfirmationChange(event) { this.setState({ passwordConfirmation: event.target.value }); }
  onTermsAcceptedChange(event) { this.setState({ termsAccepted: !this.state.termsAccepted }); }

  checkLoginAvailability() {
    const { username } = this.state;
    if (!validator.isLength(username, { min: 6, max: 20 })) {
      this.setState({ error: { field: 'username', info: 'Nazwa użytkownika powinna mieć długość 6-20 znaków.' } });
      return;
    } else if (!validator.isAlphanumeric(username, ['pl-PL'])) {
      this.setState({ error: { field: 'username', info: 'Nazwa użytkownika może składać się jedynie z liter i cyfr.' } });
      return;
    }
    this.setState({ checkingUsername: true });
    runFunctionAnonymusly('checkLoginAvailability', { username: this.state.username }, (json) => {
      if (json.ok) {
        this.setState({ checkingUsername: false, loginAvailable: true, error: { field: '', info: '' } });
      } else if (!json.ok && json.error === 'NOT_AVAILABLE') {
        this.setState({ checkingUsername: false, loginAvailable: false, error: { field: 'username', info: 'Nazwa użytkownika niedostępna' } });
      } else {
        console.error(json);
      }
    });
  }

  validate() {
    const { username, email, password, passwordConfirmation, loginAvailable, termsAccepted } = this.state;
    let ok = true;
    if (!validator.isLength(username, { min: 6, max: 20 })) {
      this.setState({ error: { field: 'username', info: 'Nazwa użytkownika powinna mieć długość 6-20 znaków.' } });
      ok = false;
    } else if (!validator.isAlphanumeric(username, ['pl-PL'])) {
      this.setState({ error: { field: 'username', info: 'Nazwa użytkownika może składać się jedynie z liter i cyfr.' } });
      ok = false;
    } else if (!loginAvailable) {
      this.setState({ error: { field: 'username', info: 'Sprawdź dostępność podanej nazwy użytkownika' } });
      ok = false;
    } else if (!validator.isEmail(email)) {
      this.setState({ error: { field: 'email', info: 'Niepoprawny adres e-mail.' } });
      ok = false;
    } else if (!validateEmailDomain(email)) {
      this.setState({ error: { field: 'email', info: 'Adres e-mail musi należeć do domeny student.uw.edu.pl lub students.mimuw.edu.pl.' } });
      ok = false;
    } else if (!validator.isLength(password, { min: 6, max: 20 })) {
      this.setState({ error: { field: 'password', info: 'Hasło powinno mieć długość 6-20 znaków.' } });
      ok = false;
    } else if (!validator.equals(password, passwordConfirmation)) {
      this.setState({ error: { field: 'passwordConfirmation', info: 'Podane hasła nie są identyczne.' } });
      ok = false;
    } else if (!termsAccepted) {
      this.setState({ error: { field: 'termsCheckbox', info: 'Musisz zaakceptować regulamin.' } });
      ok = false;
    }
    return ok;
  }

  createUser() {
    this.setState({ error: { field: null, info: '' } });
    if (this.validate()) {
      firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((user) => {
        user.updateProfile({
          displayName: this.state.username
        })
          .then(() => {
            user.sendEmailVerification();
          })
          .then(() => {
            runFunction('setLogin', { username: this.state.username }, () => { }, user);
          })
          .catch((error) => { console.error(error.message) });
        this.props.onClose();
      })
        .catch((error) => {
          var errorCode = error.code;
          if (errorCode === 'auth/email-already-in-use') {
            this.setState({ error: { field: 'none', info: 'Ten adres e-mail jest przypisany do innego konta' } });
          }
          console.error(error);
        });
    }
  }

  renderForm() {
    const { username, email, password, passwordConfirmation, error, loginAvailable, termsAccepted, checkingUsername } = this.state;
    const spinner = <Spinner className='spinner-center pt-small' />;
    const checkLoginButton = checkingUsername ? spinner : <Button className='pt-fill' onClick={this.checkLoginAvailability.bind(this)} text="Sprawdź dostępność" />;
    const loginAvailableCallout = <div className="pt-callout pt-intent-success">Nazwa użytkownika dostępna</div>;

    return <div className='pt-dialog-body'>
      <div className='pt-input-group'>
        <input type='text' className={'pt-input' + (error.field === 'username' ? ' pt-intent-danger' : '')}
          placeholder='Nazwa użytkownika' value={username} onChange={this.onUsernameChange.bind(this)} />
        <span className='pt-icon pt-icon-user' />
      </div>
      {loginAvailable ? loginAvailableCallout : checkLoginButton}
      <br />
      <div className='pt-input-group'>
        <input type='text' className={'pt-input' + (error.field === 'email' ? ' pt-intent-danger' : '')}
          placeholder='Adres e-mail' value={email} onChange={this.onEmailChange.bind(this)} />
        <span className='pt-icon pt-icon-envelope' />
      </div>
      <br />
      <div className='pt-input-group'>
        <input type='password' className={'pt-input' + ((error.field === 'passwordConfirmation' || error.field === 'password') ? ' pt-intent-danger' : '')}
          placeholder='Hasło' value={password} onChange={this.onPasswordChange.bind(this)} />
        <span className='pt-icon pt-icon-lock' />
      </div>
      <div className='pt-input-group'>
        <input type='password' className={'pt-input' + (error.field === 'passwordConfirmation' ? ' pt-intent-danger' : '')}
          placeholder='Potwierdź hasło' value={passwordConfirmation} onChange={this.onPasswordConfirmationChange.bind(this)} />
        <span className='pt-icon pt-icon-lock' />
      </div>
      <Checkbox checked={termsAccepted}
        className='default-checkbox'
        label={<span>Znam i akceptuje  <a href={terms}>regulamin</a></span>}
        onChange={this.onTermsAcceptedChange.bind(this)} />
      {error.field ? <div className="pt-callout pt-intent-danger">{error.info}</div> : ''}
    </div>;
  }

  renderFooter() {
    return <div className="pt-dialog-footer">
      <div className="pt-dialog-footer-actions">
        <Button className='default-button' onClick={this.createUser.bind(this)} text="Utwórz konto" />
      </div>
    </div>;
  }

  render() {
    return (
      <Dialog isOpen={this.props.open} onClose={this.props.onClose} title='Rejestracja' className='register-dialog'>
        {this.renderForm()}
        {this.renderFooter()}
      </Dialog>
    );
  }
}

const validateEmailDomain = (email) => {
  const allowedDomains = ['student.uw.edu.pl', 'students.mimuw.edu.pl'];
  let emailDomain = email.split('@')[1];
  return _.indexOf(allowedDomains, emailDomain) >= 0;
}
