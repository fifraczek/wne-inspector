import * as firebase from "firebase";
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';
import Home from './home/Home';
import Login from './login/Login';
import UserSettings from './userSettings/UserSettings';
import Course from './course/Course';
import {Toast} from './components/toast/Toast';
import TopBar from './components/topBar/TopBar';
import Decoration from './global/special/Decoration';

export default class App extends Component {

  loginRouter() {
    return <Router>
      <div>
        <Route path="/login" component={Login} />
        <Redirect to='/login'/>
      </div>
    </Router>;
  }

  standardRouter() {
    return <Router>
      <div>
        <TopBar user={this.props.currentUser}/><Switch> 
        <Route exact path="/" component={Home} />
        <Route exact path="/settings" component={UserSettings} />
        <Route exact path="/course/:code" component={Course} />
        <Redirect to='/'/>
        <Redirect push from='/login' to='/'/> </Switch>
        <Decoration />
      </div>
    </Router>;
  }

  emailVerified(user) {
    if(user && user.emailVerified) {
      return true;
    } else if (user && !user.emailVerified) {
      user.sendEmailVerification();
      firebase.auth().signOut();
      Toast.show({message: 'Kliknij w link aktywacyjny otrzymany mailem'});
      return false;
    }
  }

  render() {
    this.emailVerified(this.props.currentUser);
    const standardRouter = this.standardRouter();
    const loginRouter = this.loginRouter();
    return (this.props.currentUser!==null ? standardRouter : loginRouter);
  }
}
