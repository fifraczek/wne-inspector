import * as firebase from "firebase";
import _ from 'lodash';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './index.css';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyCLsqLaoQMp0kIjFSk5yQ7L0P7S8QVzt3Y",
  authDomain: "wne-inspector.firebaseapp.com",
  databaseURL: "https://wne-inspector.firebaseio.com",
  projectId: "wne-inspector",
  storageBucket: "wne-inspector.appspot.com",
  messagingSenderId: "843206843438"
};
firebase.initializeApp(config);

const displayNameFix = (user) => {
  if (_.isNil(user.displayName)) {
    const userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/usersMap/' + userId).once('value').then((snapshot) => {
      user.updateProfile({
        displayName: snapshot.val()
      });
    });
  }
}

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      displayNameFix(user);
    }
  ReactDOM.render(<App currentUser={user} />, document.getElementById('root'));
});

