import { Position, Toaster } from "@blueprintjs/core";

export const Toast = Toaster.create({
    className: "wne-toaster",
    position: Position.BOTTOM
});
