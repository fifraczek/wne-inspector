import React, { Component } from 'react';
import {Dialog, Button} from '@blueprintjs/core';

export default class ConfirmDialog extends Component {
  render() {
    return (<Dialog iconName={this.props.icon} isOpen={this.props.isOpen}
    onClose={this.props.close} title={this.props.title}>
      <div className="pt-dialog-body">
          {this.props.text}
      </div>
      <div className="pt-dialog-footer">
          <div className="pt-dialog-footer-actions">
            <Button text="Anuluj" onClick={this.props.close}/>
            <Button intent={this.props.buttonIntent} onClick={this.props.confirmAction} text={this.props.buttonText}/>
          </div>
      </div>
    </Dialog>);
  }
}
