import './top-bar.css';

import * as firebase from "firebase";
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from '@blueprintjs/core';
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import DecorationTopBar from '../../global/special/DecorationTopBar';

export default class TopBar extends Component {

  singOut() {
    firebase.auth().signOut();
  }

  render() {
    return (
      <Navbar inverse collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/' className='top-bar-title'>WNE Inspector</Link>
          </Navbar.Brand>
          <DecorationTopBar />
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <LinkContainer to="/settings">
              <NavItem eventKey={1}>
                <Icon iconName="user" /><span className='top-bar-element'>{this.props.user.displayName}</span>
              </NavItem>
            </LinkContainer>
            <NavItem eventKey={2} onClick={this.singOut}>
              <Icon iconName="log-out" /><span className='top-bar-element'>Wyloguj</span>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

