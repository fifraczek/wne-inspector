import React, { Component } from 'react';
import { Spinner } from '@blueprintjs/core';
import './loading.css';

export default class Loading extends Component {

    render() {
        return (<div className='loading-container'>
            <Spinner className='pt-large custom-spinner'/>  
            </div>
        );
    }
}