import React, { Component } from 'react';

export default class UnderConstruction extends Component {
    render() {
        return (
        <div className="pt-non-ideal-state">
            <div className="pt-non-ideal-state-visual pt-non-ideal-state-icon">
                <span className="pt-icon pt-icon-build" style={{ color: 'white', fontSize: '7.5rem' }}></span>
            </div>
            <h4 className="pt-non-ideal-state-title">Strona w budowie</h4>
            <div className="pt-non-ideal-state-description">
                Ta strona jest w trakcie budowy, będzie dostępna wkrótce
            </div>
        </div>);
    }
}
