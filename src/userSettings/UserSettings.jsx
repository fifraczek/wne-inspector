import './user-settings.css';

import * as firebase from 'firebase';
import React, { Component } from 'react';
import { Button, Intent, Icon } from '@blueprintjs/core';
import validator from 'validator';
import { Toast } from '../components/toast/Toast';
import ConfirmDialog from '../components/confirmDialog/ConfirmDialog';

export default class UserSettings extends Component {
  constructor(props) {
    super(props);
    this.state = this.getDefaultState();
  }

  getDefaultState() {
    return {
      password: '',
      passwordConfirmation: '',
      error: { field: '', info: '' },
      deleteConfirmOpen: false
    };
  }

  onPasswordChange(event) { this.setState({ password: event.target.value }); }
  onPasswordConfirmationChange(event) { this.setState({ passwordConfirmation: event.target.value }); }

  toggleDeleteDialog() {
    this.setState({ deleteConfirmOpen: !this.state.deleteConfirmOpen });
  }

  deleteUser() {
    firebase.auth().currentUser.reload().then(() => {
      firebase.auth().currentUser.delete();
    });
  }

  updatePassword() {
    if (this.validate()) {
      firebase.auth().currentUser.updatePassword(this.state.password).then(() => {
        this.setState(this.getDefaultState());
        Toast.show({ message: 'Hasło zostało zmienione.', intent: Intent.SUCCESS });
      })
        .catch((error) => {
          Toast.show({ message: 'Wystąpił błąd.', intent: Intent.DANGER });
        });
    }
  }

  validate() {
    const { password, passwordConfirmation } = this.state;
    let ok = true;
    if (!validator.isLength(password, { min: 6, max: 20 })) {
      this.setState({ error: { field: 'password', info: 'Hasło powinno mieć długość 6-20 znaków.' } });
      ok = false;
    } else if (!validator.equals(password, passwordConfirmation)) {
      this.setState({ error: { field: 'passwordConfirmation', info: 'Podane hasła nie są identyczne.' } });
      ok = false;
    }
    return ok;
  }

  render() {
    const { password, passwordConfirmation, error } = this.state;
    return (
      <div className='user-settings-page'>
        <h3>Ustawienia użytkownika</h3>
        <div className='pt-form-group'>
          <label className='pt-label'>Zmień hasło</label>
          <div className='pt-form-content'>
            <div className='pt-input-group'>
              <input type='password' className={'pt-input' + ((error.field === 'passwordConfirmation' || error.field === 'password') ? ' pt-intent-danger' : '')}
                placeholder='Hasło' value={password} onChange={this.onPasswordChange.bind(this)} autoComplete='off' />
              <span className='pt-icon pt-icon-lock' />
            </div>
            <div className='pt-input-group'>
              <input type='password' className={'pt-input' + (error.field === 'passwordConfirmation' ? ' pt-intent-danger' : '')}
                placeholder='Potwierdź hasło' value={passwordConfirmation} onChange={this.onPasswordConfirmationChange.bind(this)} autoComplete='off' />
              <span className='pt-icon pt-icon-lock' />
            </div>
            {error.field ? <div className='pt-callout pt-intent-danger'>{error.info}</div> : ''}
            <div className='pt-form-helper-text'>Hasło musi mieć długość 6-20 znaków</div>
            <Button className='pt-fill' onClick={this.updatePassword.bind(this)} text='Aktualizuj' />
          </div>
          <br />
          <Button className='pt-fill pt-icon-delete' intent={Intent.DANGER} onClick={this.toggleDeleteDialog.bind(this)} text='Usuń konto' />
          <a href='https://bitbucket.org/fifraczek/wne-inspector/issues'><Icon iconName='issue' />Zgłoś błąd na stronie</a>
        </div>
        <ConfirmDialog isOpen={this.state.deleteConfirmOpen} icon='warning-sign' title='Uwaga!' text='Czy na pewno chcesz usunąć swoje konto?'
          buttonIntent={Intent.DANGER} buttonText='Usuń' close={this.toggleDeleteDialog.bind(this)} confirmAction={this.deleteUser} />
      </div>
    );
  }
}
