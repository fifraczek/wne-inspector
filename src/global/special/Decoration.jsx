import './decoration.css';
import React, { Component } from 'react';
import Snowflakes from 'react-snowflakes';

export default class Decoration extends Component {
    renderChristmas() {
        return <div className='snowflakes-animation'>
            <Snowflakes numberOfSnowflakes={50} snowflakeColor="rgba(255,255,255,.2)" snowflakeChar="*" />
        </div>;
    }

    render() {
        let currentDate = new Date();
        if (isChristmas(currentDate)) {
            return this.renderChristmas();
        }
        return null;
    }
}

function isChristmas(date) {
    return (date.getMonth() === 11 && date.getDate() >= 6) || (date.getMonth() === 0 && date.getDate() <= 6);
}