import './decoration.css';
import React, { Component } from 'react';
import christmasIcon from '../../resources/images/christmas-icon.svg'

export default class Decoration extends Component {
    renderChristmas() {
        return <img src={christmasIcon} className='christmas-top' alt='xmas'/>;
    }

    render() {
        let currentDate = new Date();
        if (isChristmas(currentDate)) {
            return this.renderChristmas();
        }
        return null;
    }
}

function isChristmas(date) {
    return (date.getMonth() === 11 && date.getDate() >= 6) || (date.getMonth() === 0 && date.getDate() <= 6);
}