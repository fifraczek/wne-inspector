import * as firebase from 'firebase';

const fileUpload = (fileObject, filePath, uploadControl, onSuccess) => {
    const storage = firebase.storage();
    const fileRef = storage.ref(filePath);

    return fileRef.getDownloadURL().then(
        () => {
            throw new Error('FILE_ALREADY_EXISTS');
        },
        () => {
            const uploadTask = fileRef.put(fileObject);
            uploadControl.task = uploadTask;
            return uploadTask.on('state_changed', (snapshot) => {
                const { state, bytesTransferred, totalBytes } = snapshot;
                uploadControl.onStateChange(state, bytesTransferred, totalBytes);
            }, (error) => {
                // TODO: smart error handling
            }, () => {
                onSuccess();
            });

        }
    );


};

export default fileUpload;