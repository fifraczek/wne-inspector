import * as firebase from 'firebase';

const runFunction = (functionName, params, onSuccess, user) => {
    // DEV ENVIRONMENT FUNCTIONS URL
    // const appURL = 'http://localhost:5000/wne-inspector/us-central1';
    const appURL = 'https://us-central1-wne-inspector.cloudfunctions.net';
    let currentUser = user ? user : firebase.auth().currentUser;
    currentUser.getIdToken(true).then((idToken) => {
        let details = {
            method: 'POST',
            body: JSON.stringify(params),
            headers: { 'Authorization': 'Bearer ' + idToken }
        };

        fetch(appURL + '/' + functionName, details)
            .then(res => res.json())
            .then(json => onSuccess(json));
    }).catch((error) => {
        console.error(error);
    });
};

export default runFunction;