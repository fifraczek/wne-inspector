const runFunctionAnonymusly = (functionName, params, onSuccess) => {
    // DEV ENVIRONMENT FUNCTIONS URL
    // const appURL = 'http://localhost:5000/wne-inspector/us-central1';
    const appURL = 'https://us-central1-wne-inspector.cloudfunctions.net';
    let details = {
        method: 'POST',
        body: JSON.stringify(params),
        headers: [
          ["Access-Control-Allow-Origin", "*"]
        ]
    };

    fetch(appURL + '/' + functionName, details)
        .then(res => res.json())
        .then(json => onSuccess(json))
        .catch((error) => {
            console.error(error);
        });
};

export default runFunctionAnonymusly;