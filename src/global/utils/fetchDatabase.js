import * as firebase from 'firebase';

const fetchDatabase = (url, onSuccess) => {
    const appURL = 'https://wne-inspector.firebaseio.com';
    firebase.auth().currentUser.getIdToken(true).then((idToken) => {
        fetch(appURL + '/' + url + '.json?auth=' + idToken)
            .then(res => res.json())
            .then(json => onSuccess(json));
    }).catch((error) => {
        console.error(error);
    });
};

export default fetchDatabase;